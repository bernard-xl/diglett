use std::error::Error;

use clap::Parser;
use cli::{print_response, Args};
use dns::{Query, Resolve, UdpResolver};

mod cli;
mod dns;

trait StringExt {
    fn complete_tailing_dot(&mut self) -> &String;
}

impl StringExt for String {
    fn complete_tailing_dot(&mut self) -> &String {
        if !self.ends_with('.') {
            self.push('.')
        }
        self
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args = Args::parse();

    let mut resolver = UdpResolver::new(&args.nameserver, args.recursion_desired)?;
    let resp = resolver.resolve(Query {
        typ: args.qtype,
        class: args.qclass,
        name: args.qname.complete_tailing_dot(),
    })?;

    print_response(args.output_fmt, resp);
    Ok(())
}
