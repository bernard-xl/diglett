use std::{env::var, fmt::Display, io::stdout, str::FromStr};

use clap::Parser;
use termion::{color, is_tty, style};

use crate::dns::{Class, Flags, ResourceRecord, Response, Type};

#[derive(Debug, Parser)]
#[command(author = "shieliang.poh")]
#[command(about = "It digs! This is a simple DNS client ;)", long_about = None)]
pub(crate) struct Args {
    /// The first nameserver to reach out to.
    #[arg(short='n', long, default_value_t=String::from("198.41.0.4:53"))]
    pub nameserver: String,

    /// Request the nameserver to perform recursive query.
    #[arg(short = 'r', long, default_value_t = false)]
    pub recursion_desired: bool,

    #[arg(short='o', long, default_value_t=OutputFormat::Text)]
    /// The output format of the result.
    pub output_fmt: OutputFormat,

    /// The type of the query.
    #[arg(short='t', long, default_value_t = Type::A)]
    pub qtype: Type,

    /// The class of the query.
    #[arg(short='c', long, default_value_t = Class::IN)]
    pub qclass: Class,

    /// The domain name.
    pub qname: String,
}

#[derive(Clone, Copy, Debug)]
pub(crate) enum OutputFormat {
    Text,
    Json,
}

impl FromStr for OutputFormat {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "text" => Ok(Self::Text),
            "json" => Ok(Self::Json),
            x => Err(format!("unrecognised output format \"{}\"", x)),
        }
    }
}

impl Display for OutputFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Text => f.pad("text"),
            Self::Json => f.pad("json"),
        }
    }
}

pub(crate) fn print_response(format: OutputFormat, resp: Response) {
    match format {
        OutputFormat::Json => println!("{}", serde_json::to_string(&resp).unwrap()),
        OutputFormat::Text => {
            let deco = if can_print_color() {
                termion_decoration()
            } else {
                noop_decoration()
            };

            print_response_text(&deco, resp)
        }
    }
}

fn can_print_color() -> bool {
    is_tty(&stdout()) && var("NO_COLOR").unwrap_or_default() == ""
}

struct Decoration {
    style: Style,
    fg: Foreground,
}

struct Style {
    bold: &'static str,
    underline: &'static str,
    reset: &'static str,
}

struct Foreground {
    red: &'static str,
    green: &'static str,
    blue: &'static str,
    yellow: &'static str,
    magenta: &'static str,
    cyan: &'static str,
    reset: &'static str,
}

fn termion_decoration() -> Decoration {
    Decoration {
        style: Style {
            bold: style::Bold.as_ref(),
            underline: style::Underline.as_ref(),
            reset: style::Reset.as_ref(),
        },
        fg: Foreground {
            red: color::Red.fg_str(),
            green: color::Green.fg_str(),
            blue: color::Blue.fg_str(),
            yellow: color::Yellow.fg_str(),
            magenta: color::Magenta.fg_str(),
            cyan: color::Cyan.fg_str(),
            reset: color::Reset.fg_str(),
        },
    }
}

fn noop_decoration() -> Decoration {
    Decoration {
        style: Style {
            bold: "",
            underline: "",
            reset: "",
        },
        fg: Foreground {
            red: "",
            green: "",
            blue: "",
            yellow: "",
            magenta: "",
            cyan: "",
            reset: "",
        },
    }
}

fn print_response_text(deco: &Decoration, resp: Response) {
    print_section_title(deco, "Header");
    if resp.flags.is_empty() {
        println!("   (nothing interesting here)");
    } else {
        let flags = resp.flags;
        let fg = &deco.fg;

        if flags.contains(Flags::RecursionAvailable) {
            println!(
                "   {}- The nameserver supports recursive query{}",
                fg.yellow, fg.reset
            );
        }
        if flags.contains(Flags::AuthoritativeAnswer) {
            println!("   {}- We got authoritative answers{}", fg.green, fg.reset);
        }
        if flags.contains(Flags::Truncation) {
            println!("   {}- The response is truncated{}", fg.red, fg.reset);
        }
    }

    print_section_title(deco, "\nAnswers");
    if resp.answers.is_empty() {
        println!("   (nope!)")
    } else {
        let longest_name = resp.answers.iter().fold(5, longer_name);
        for ans in resp.answers.iter() {
            print_resource_record(deco, longest_name, ans);
        }
    }

    if !resp.authorities.is_empty() {
        print_section_title(deco, "\nAuthorities");

        let longest_name = resp.authorities.iter().fold(5, longer_name);
        for auth in resp.authorities.iter() {
            print_resource_record(deco, longest_name, auth);
        }
    }

    if !resp.additional.is_empty() {
        print_section_title(deco, "\nAdditional");

        let longest_name = resp.additional.iter().fold(5, longer_name);
        for add in resp.additional.iter() {
            print_resource_record(deco, longest_name, add);
        }
    }
}

fn print_section_title(deco: &Decoration, s: &'static str) {
    let style = &deco.style;
    println!("{}{}{}{}", style.underline, style.bold, s, style.reset)
}

fn print_resource_record(deco: &Decoration, longest_name: usize, rr: &ResourceRecord) {
    let fg = &deco.fg;
    println!(
        "   {}{:longest_name$}   {}{:7}   {}{:5}   {}{:6}   {}{}{}",
        fg.green,
        rr.name,
        fg.blue,
        rr.ttl.as_secs(),
        fg.magenta,
        rr.class,
        fg.yellow,
        rr.data.resource_type(),
        fg.cyan,
        rr.data,
        fg.reset,
        longest_name = longest_name,
    );
}

fn longer_name(acc: usize, rr: &ResourceRecord) -> usize {
    acc.max(rr.name.len())
}
