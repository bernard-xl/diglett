use std::{
    borrow::Borrow,
    fmt::Write as FmtWrite,
    io::{self, Read, Write},
    net::{Ipv4Addr, Ipv6Addr},
    time::Duration,
};

use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

use super::{model::{Class, Error, Flags, Query, ResourceData, ResourceRecord, Response, Type}};

pub(super) trait WriteExt {
    fn write_query<'a>(&mut self, id: u16, flags: u16, q: impl Borrow<Query<'a>>) -> Result<(), Error>;
}

impl<T: Write> WriteExt for T {
    fn write_query<'a>(&mut self, id: u16, flags: u16, q: impl Borrow<Query<'a>>) -> Result<(), Error> {
        // write header
        for v in [id, flags, 1, 0, 0, 0] {
            self.write_u16::<BigEndian>(v)?;
        }
        // write question
        let q = q.borrow();
        write_str(self, q.name)?;
        self.write_u16::<BigEndian>(q.typ.into())?;
        self.write_u16::<BigEndian>(q.class.into())?;
        Ok(())
    }
}

pub(super) trait ReadExt {
    fn read_response(&mut self) -> Result<(Header, Response), Error>;
}

impl ReadExt for &[u8] {
    fn read_response(&mut self) -> Result<(Header, Response), Error> {
        let mut reader = *self;
        let ctab = *self;

        let header = read_header(&mut reader)?;
        if let Some(err) = header.rcode.into() {
            return Err(err);
        }

        skip_questions(&mut reader, header.question_count)?;

        let flags = header.flags;
        let answers = read_records(&mut reader, ctab, header.answer_count).unwrap_or_default();
        let authorities = read_records(&mut reader, ctab, header.authority_count).unwrap_or_default();
        let additional = read_records(&mut reader, ctab, header.additional_count).unwrap_or_default();

        let response = Response { flags, answers, authorities, additional };

        Ok((header, response))
    }
}

fn write_str<W: Write>(w: &mut W, s: &str) -> io::Result<()> {
    for label in s.split(".") {
        w.write_u8(label.len() as u8)?;
        write!(w, "{}", label)?;
    }
    Ok(())
}

#[derive(Debug)]
pub(super) struct Header {
    id: u16,
    flags: Flags,
    rcode: ResponseCode,
    question_count: u16,
    answer_count: u16,
    authority_count: u16,
    additional_count: u16,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub(super) struct ResponseCode(u16);

impl Into<Option<Error>> for ResponseCode {
    fn into(self) -> Option<Error> {
        match self.0 {
            0 => None,
            1 => Some(Error::FormatError),
            2 => Some(Error::ServerFailure),
            3 => Some(Error::NameError),
            4 => Some(Error::NotImplemented),
            5 => Some(Error::Refused),
            x => Some(Error::UnexpectedField("response code", x)),
        }
    }
}

fn read_header(r: &mut &[u8]) -> Result<Header, io::Error> {
    if r.len() < 12 {
        return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "buffer less than 12 bytes"));
    }

    let id = r.read_u16::<BigEndian>()?;
    let fields = r.read_u16::<BigEndian>()?;
    let flags = Flags::from_bits_retain(fields & 0x0780);
    let rcode = ResponseCode(fields & 0x000f);
    let question_count = r.read_u16::<BigEndian>()?;
    let answer_count = r.read_u16::<BigEndian>()?;
    let authority_count = r.read_u16::<BigEndian>()?;
    let additional_count = r.read_u16::<BigEndian>()?;

    Ok(Header { id, flags, rcode, question_count, answer_count, authority_count, additional_count })
}

fn skip_questions(r: &mut &[u8], count: u16) -> Result<(), io::Error> {
    for _ in 0..count {
        skip_str(r)?;
        *r = &r[4..];
    }
    Ok(())
}

fn skip_str(r: &mut &[u8]) -> Result<(), io::Error> {
    loop {
        let length = r.read_u8()? as usize;
        if length == 0 {
            break;
        }
        *r = &r[length..];
    }
    Ok(())
}

fn read_records(r: &mut &[u8], ctab: &[u8], count: u16) -> Result<Vec<ResourceRecord>, Error> {
    let mut records = Vec::<ResourceRecord>::new();
    records.reserve(count as usize);

    for _ in 0..count {
        let name = read_name(r, ctab)?;
        let typ = Type::from(r.read_u16::<BigEndian>()?);
        let class = Class::from(r.read_u16::<BigEndian>()?);
        let ttl = Duration::from_secs(r.read_u32::<BigEndian>()? as u64);
        let data = read_data(r, ctab, typ)?;

        records.push(ResourceRecord { name, class, ttl, data });
    }
    Ok(records)
}

fn read_name(r: &mut &[u8], ctab: &[u8]) -> Result<String, Error> {
    read_name_rec(r, ctab, String::new())
}

fn read_name_rec(r: &mut &[u8], ctab: &[u8], mut s: String) -> Result<String, Error> {
    let length = r.read_u8()? as usize;
    if length == 0 {
        Ok(s)
    } else if length & 0xc0 == 0 {
        (&r[..length]).read_to_string(&mut s)?;
        *r = &r[length..];
        s.push('.');
        read_name_rec(r, ctab, s)
    } else if length & 0xc0 == 0xc0 {
        let offset = ((length & 0x3f) << 8) | (r.read_u8()? as usize);
        let d = &mut &ctab[offset..];
        read_name_rec(d, ctab, s)
    } else {
        Err(Error::UnexpectedField("label directive", (length & 0xc0) as u16))
    }
}

fn read_str(r: &mut &[u8]) -> Result<String, Error> {
    read_str_rec(r, String::new())
}

fn read_str_rec(r: &mut &[u8], mut s: String) -> Result<String, Error> {
    if r.is_empty() {
        return Ok(s);
    }

    let length = r.read_u8()? as usize;
    s.reserve(length);

    (&r[..length]).read_to_string(&mut s)?;
    *r = &r[length..];

    read_str_rec(r, s)
}

fn read_str_once(r: &mut &[u8]) -> Result<String, Error> {
    let length = r.read_u8()? as usize;

    let mut s = String::new();
    s.reserve(length);

    (&r[..length]).read_to_string(&mut s)?;
    *r = &r[length..];

    Ok(s)
}

fn read_hex(r: &mut &[u8]) -> Result<String, Error> {
    let mut s = String::new();
    s.reserve(r.len());

    for x in r.into_iter() {
        write!(s, "{:X}", x).unwrap();
    }
    *r = &r[r.len()..];
    Ok(s)    
}

fn read_data(r: &mut &[u8], ctab: &[u8], typ: Type) -> Result<ResourceData, Error> {
    let length = r.read_u16::<BigEndian>()? as usize;
    let mut lr = &r[..length];
    *r = &r[length..];

    match typ {
        Type::A => read_ipv4(&mut lr).map(ResourceData::A),
        Type::NS => read_name(&mut lr, ctab).map(ResourceData::NS),
        Type::CNAME => read_name(&mut lr, ctab).map(ResourceData::CNAME),
        Type::SOA => read_rd_soa(&mut lr, ctab),
        Type::PTR => read_name(&mut lr, ctab).map(ResourceData::PTR),
        Type::SRV => read_rd_srv(&mut lr, ctab),
        Type::MX => read_rd_mx(&mut lr, ctab),
        Type::TXT => read_str(&mut lr).map(ResourceData::TXT),
        Type::AAAA => read_ipv6(&mut lr).map(ResourceData::AAAA),
        Type::SSHFP => read_rd_sshfp(&mut lr),
        Type::CAA => read_rd_caa(&mut lr),
        _ => read_rd_raw(&mut lr, typ, length),
    }
}

fn read_ipv4(r: &mut &[u8]) -> Result<Ipv4Addr, Error> {
    let raw = r.read_u32::<BigEndian>()?;
    Ok(Ipv4Addr::from(raw))
}

fn read_ipv6(r: &mut &[u8]) -> Result<Ipv6Addr, Error> {
    let raw = r.read_u128::<BigEndian>()?;
    Ok(Ipv6Addr::from(raw))
}

fn read_rd_srv(r: &mut &[u8], ctab: &[u8]) -> Result<ResourceData, Error> {
    let priority = r.read_u16::<BigEndian>()?;
    let weight = r.read_u16::<BigEndian>()?;
    let port = r.read_u16::<BigEndian>()?;
    let target = read_name(r, ctab)?;

    Ok(ResourceData::SRV { priority, weight, port, target })
}

fn read_rd_mx(r: &mut &[u8], ctab: &[u8]) -> Result<ResourceData, Error> {
    let preference = r.read_u16::<BigEndian>()?;
    let exchange = read_name(r, ctab)?;
    Ok(ResourceData::MX { preference, exchange })
}

fn read_rd_soa(r: &mut &[u8], ctab: &[u8]) -> Result<ResourceData, Error> {
    let mname = read_name(r, ctab)?;
    let rname = read_name(r, ctab)?;
    let serial = r.read_u32::<BigEndian>()?;
    let refresh = r.read_u32::<BigEndian>()?;
    let retry = r.read_u32::<BigEndian>()?;
    let expire = r.read_u32::<BigEndian>()?;
    let minimum = r.read_u32::<BigEndian>()?;

    Ok(ResourceData::SOA { mname, rname, serial, refresh, retry, expire, minimum })
}

fn read_rd_sshfp(r: &mut &[u8]) -> Result<ResourceData, Error> {
    let algorithm = r.read_u8()?;
    let fptype = r.read_u8()?;
    let fingerprint = read_hex(r)?;

    Ok(ResourceData::SSHFP { algorithm, fptype, fingerprint })
}

fn read_rd_caa(r: &mut &[u8]) -> Result<ResourceData, Error> {
    let flags = r.read_u8()?;
    let tag = read_str_once(r)?;

    let mut value = String::new();
    r.read_to_string(&mut value)?;

    Ok(ResourceData::CAA { flags, tag, value })
}

fn read_rd_raw(r: &mut &[u8], typ: Type, length: usize) -> Result<ResourceData, Error> {
    let length = length.min(r.len());
    let data = Vec::from(&r[..length]);
    *r = &r[..length];
    Ok(ResourceData::Raw { typ, data })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_write_str() {
        let mut buf = Vec::<u8>::new();
        write_str(&mut buf, "example.com.").unwrap();
        assert_eq!(&buf[..], b"\x07example\x03com\x00");
    }

    #[test]
    fn test_skip_str() {
        let buf = b"\x07example\x03com\x00";
        let mut reader = &buf[..];
        skip_str(&mut reader).unwrap();
        assert_eq!(reader.len(), 0);
    }

    #[test]
    fn test_read_str_simple() {
        let buf = b"\x07example\x03com\x00";
        let s = read_name(&mut &buf[..], &buf[..]).unwrap();
        assert_eq!(&s, "example.com.");
    }

    #[test]
    fn test_read_str_compressed() {
        let buf = b"\x07example\x03com\x00\x04test\xc0\x00";
        let s = read_name(&mut &buf[13..], &buf[..]).unwrap();
        assert_eq!(&s, "test.example.com.");
    }

    #[test]
    fn test_read_header() {
        let buf: [u8; 12] = [35, 51, 130, 0, 0, 1, 0, 0, 0, 13, 0, 11];
        let header = read_header(&mut &buf[..]).unwrap();
        assert_eq!(9011, header.id);
        assert_eq!(Flags::Truncation, header.flags);
        assert_eq!(ResponseCode(0), header.rcode);
        assert_eq!(1, header.question_count);
        assert_eq!(0, header.answer_count);
        assert_eq!(13, header.authority_count);
        assert_eq!(11, header.additional_count);
    }

    #[test]
    fn test_skip_questions() {
        let buf: [u8; 17] = [
            7, 116, 119, 105, 116, 116, 101, 114, 3, 99, 111, 109, 0, 0, 1, 0, 1,
        ];
        let mut reader = &buf[..];
        skip_questions(&mut reader, 1).unwrap();
        assert_eq!(0, reader.len());
    }

    #[test]
    fn test_read_record_answer() {
        let buf: [u8; 45] = [
            35, 51, 132, 0, 0, 1, 0, 1, 0, 8, 0, 0, 7, 116, 119, 105, 116, 116, 101, 114, 3, 99,
            111, 109, 0, 0, 1, 0, 1, 192, 12, 0, 1, 0, 1, 0, 0, 7, 8, 0, 4, 104, 244, 42, 129,
        ];
        let ctab = &buf[..];
        let mut ans = &buf[29..];
        let answers = read_records(&mut ans, ctab, 1).unwrap();

        assert_eq!(1, answers.len());
        assert_eq!("twitter.com.", answers[0].name);
        assert_eq!(Class::IN, answers[0].class);
        assert_eq!(
            ResourceData::A(Ipv4Addr::new(104, 244, 42, 129)),
            answers[0].data
        );
    }
}
