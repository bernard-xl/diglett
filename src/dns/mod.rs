use std::borrow::Borrow;
use std::io;
use std::net::{SocketAddr, SocketAddrV4, ToSocketAddrs, UdpSocket};

use ioext::{ReadExt, WriteExt};
pub use model::*;

mod ioext;
mod model;

pub trait Resolve {
    fn resolve<'a>(&mut self, q: impl Borrow<Query<'a>>) -> Result<Response, Error>;
}

pub struct UdpResolver {
    socket: UdpSocket,
    nameserver: SocketAddr,
    flags: u16,
    id: u16,
}

impl UdpResolver {
    pub fn new(nameserver: impl ToSocketAddrs, rd: bool) -> Result<Self, io::Error> {
        let socket = UdpSocket::bind("0.0.0.0:0")?;
        let nameserver = nameserver.to_socket_addrs()?.next().ok_or_else(|| {
            io::Error::new(io::ErrorKind::InvalidInput, "empty nameserver address")
        })?;
        let flags = if rd { 0x0001 } else { 0x0000 };
        let id = 2333;

        Ok(Self {
            socket,
            nameserver,
            flags,
            id,
        })
    }

    fn resolve_rec<'a>(&mut self, buf: &mut [u8], ns: SocketAddr, q: impl Borrow<Query<'a>>) -> Result<Response, Error> {
        let mut txbuf = &mut buf[..];
        let id = self.next_id();
        txbuf.write_query(id, self.flags, q.borrow())?;
        let written_bytes = 512 - txbuf.len();

        self.socket.send_to(&buf[..written_bytes], ns)?;

        let mut rxbuf = &mut buf[..];
        let (recv_bytes, _) = self.socket.recv_from(&mut rxbuf)?;
        let (_, response) = (&mut &rxbuf[..recv_bytes]).read_response()?;

        if response.answers.is_empty() {
            if !response.additional.is_empty() {
                let next_ns_addr = response.additional.iter().find_map(into_socket_addr);

                if let Some(addr) = next_ns_addr {
                    return self.resolve_rec(buf, addr, q);
                }
            }

            if !response.authorities.is_empty() {
                let next_ns_name = response.authorities.iter().find_map(into_domain_name);

                if let Some(name) = next_ns_name {
                    let next_ns_resp = self.resolve_rec(
                        buf,
                        self.nameserver,
                        Query {
                            typ: Type::A,
                            class: Class::IN,
                            name: &name,
                        },
                    )?;
                    let next_ns_addr = next_ns_resp.answers.iter().find_map(into_socket_addr);

                    if let Some(addr) = next_ns_addr {
                        return self.resolve_rec(buf, addr, q);
                    }
                }
            }
        }

        Ok(response)
    }

    fn next_id(&mut self) -> u16 {
        self.id = self.id.wrapping_add(1);
        self.id
    }
}

impl Resolve for UdpResolver {
    fn resolve<'a>(&mut self, q: impl Borrow<Query<'a>>) -> Result<Response, Error> {
        let mut buf = [0; 512];
        self.resolve_rec(&mut buf, self.nameserver, q)
    }
}

fn into_socket_addr(rr: &ResourceRecord) -> Option<SocketAddr> {
    match rr.data {
        ResourceData::A(ipv4) => Some(SocketAddr::V4(SocketAddrV4::new(ipv4, 53))),
        // ResourceData::AAAA(ipv6) => Some(SocketAddr::V6(SocketAddrV6::new(ipv6, 53, 0, 0))),
        _ => None,
    }
}

fn into_domain_name(rr: &ResourceRecord) -> Option<&str> {
    match &rr.data {
        ResourceData::NS(name) => Some(name),
        _ => None,
    }
}
