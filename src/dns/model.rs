use std::{
    fmt::Display,
    io,
    net::{Ipv4Addr, Ipv6Addr},
    str::FromStr,
    time::Duration,
};

use bitflags::bitflags;
use hex::ToHex;
use serde::{ser::SerializeStruct, Serialize, Serializer};
use thiserror::Error;

#[derive(Debug)]
pub struct Query<'a> {
    pub typ: Type,
    pub class: Class,
    pub name: &'a str,
}

#[derive(Debug, Serialize)]
pub struct Response {
    pub flags: Flags,
    pub answers: Vec<ResourceRecord>,
    pub authorities: Vec<ResourceRecord>,
    pub additional: Vec<ResourceRecord>,
}

#[derive(Debug, Serialize)]
pub struct ResourceRecord {
    pub name: String,
    pub class: Class,
    #[serde(serialize_with = "duration_sec")]
    pub ttl: Duration,
    #[serde(flatten)]
    pub data: ResourceData,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize)]
#[repr(u16)]
pub enum Type {
    // Meta
    NS = 2,
    CNAME = 5,
    DNAME = 39,
    PTR = 12,
    NAPTR = 35,
    OPT = 41,
    CSYNC = 62,
    IXFR = 251,
    AXFR = 252,
    TKEY = 249,
    TSIG = 250,
    ZONEMD = 63,

    // DNSSEC
    DNSKEY = 48,
    CDNSKEY = 60,
    RRSIG = 46,
    NSEC = 47,
    NSEC3 = 50,
    NSEC3PARAM = 51,
    DS = 43,
    CDS = 59,
    TA = 32768,
    DLV = 32769,

    // IP
    A = 1,
    AAAA = 28,
    APL = 42,
    DHCID = 49,
    HIP = 55,
    IPSECKEY = 45,

    // Service discovery
    SRV = 33,

    // Email
    MX = 15,
    SMIMEA = 53,
    OPENPGPKEY = 61,

    // Informational
    TXT = 16,
    HINFO = 13,
    RP = 17,
    LOC = 29,

    // Security
    SSHFP = 44,
    TLSA = 52,
    CERT = 37,
    KX = 36,
    CAA = 257,

    // Misc.
    MD = 3,
    MF = 4,
    SOA = 6,
    MB = 7,
    MG = 8,
    MR = 9,
    NULL = 10,
    WKS = 11,
    MINFO = 14,

    // Wildcard
    Any = 255,

    // Unknown
    Unknown(u16),
}

impl Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            // Meta
            Self::NS => f.pad("CNAME"),
            Self::CNAME => f.pad("CNAME"),
            Self::DNAME => f.pad("DNAME"),
            Self::PTR => f.pad("PTR"),
            Self::NAPTR => f.pad("NAPTR"),
            Self::OPT => f.pad("OPT"),
            Self::CSYNC => f.pad("CSYNC"),
            Self::IXFR => f.pad("IXFR"),
            Self::AXFR => f.pad("AXFR"),
            Self::TKEY => f.pad("TKEY"),
            Self::TSIG => f.pad("TSIG"),
            Self::ZONEMD => f.pad("ZONEMD"),

            // DNSSEC
            Self::DNSKEY => f.pad("DNSKEY"),
            Self::CDNSKEY => f.pad("CDNSKEY"),
            Self::RRSIG => f.pad("RRSIG"),
            Self::NSEC => f.pad("NSEC"),
            Self::NSEC3 => f.pad("NSEC3"),
            Self::NSEC3PARAM => f.pad("NSEC3PARAM"),
            Self::DS => f.pad("DS"),
            Self::CDS => f.pad("CDS"),
            Self::TA => f.pad("TA"),
            Self::DLV => f.pad("DLV"),

            // IP
            Self::A => f.pad("A"),
            Self::AAAA => f.pad("AAAA"),
            Self::APL => f.pad("APL"),
            Self::DHCID => f.pad("DHCID"),
            Self::HIP => f.pad("HIP"),
            Self::IPSECKEY => f.pad("IPSECKEY"),

            // Service discovery
            Self::SRV => f.pad("SRV"),

            // Email
            Self::MX => f.pad("MX"),
            Self::SMIMEA => f.pad("SMIMEA"),
            Self::OPENPGPKEY => f.pad("OPENPGPKEY"),

            // Informational
            Self::TXT => f.pad("TXT"),
            Self::HINFO => f.pad("HINFO"),
            Self::RP => f.pad("RP"),
            Self::LOC => f.pad("LOC"),

            // Security
            Self::SSHFP => f.pad("SSHFP"),
            Self::TLSA => f.pad("TLSA"),
            Self::CERT => f.pad("CERT"),
            Self::KX => f.pad("KX"),
            Self::CAA => f.pad("CAA"),

            // Misc.
            Self::MD => f.pad("MD"),
            Self::MF => f.pad("MF"),
            Self::SOA => f.pad("SOA"),
            Self::MB => f.pad("MB"),
            Self::MG => f.pad("MG"),
            Self::MR => f.pad("MR"),
            Self::NULL => f.pad("NULL"),
            Self::WKS => f.pad("WKS"),
            Self::MINFO => f.pad("MINFO"),

            // Wildcard
            Self::Any => f.pad("*"),

            // Unknown
            Self::Unknown(x) => f.pad(&x.to_string()),
        }
    }
}

impl FromStr for Type {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            // Meta
            "NS" => Ok(Self::NS),
            "CNAME" => Ok(Self::CNAME),
            "DNAME" => Ok(Self::DNAME),
            "PTR" => Ok(Self::PTR),
            "NAPTR" => Ok(Self::NAPTR),
            "OPT" => Ok(Self::OPT),
            "CSYNC" => Ok(Self::CSYNC),
            "IXFR" => Ok(Self::IXFR),
            "AXFR" => Ok(Self::AXFR),
            "TKEY" => Ok(Self::TKEY),
            "TSIG" => Ok(Self::TSIG),
            "ZONEMD" => Ok(Self::ZONEMD),

            // DNSSEC
            "DNSKEY" => Ok(Self::DNSKEY),
            "CDNSKEY" => Ok(Self::CDNSKEY),
            "RRSIG" => Ok(Self::RRSIG),
            "NSEC" => Ok(Self::NSEC),
            "NSEC3" => Ok(Self::NSEC3),
            "NSEC3PARAM" => Ok(Self::NSEC3PARAM),
            "DS" => Ok(Self::DS),
            "CDS" => Ok(Self::CDS),
            "TA" => Ok(Self::TA),
            "DLV" => Ok(Self::DLV),

            // IP
            "A" => Ok(Self::A),
            "AAAA" => Ok(Self::AAAA),
            "APL" => Ok(Self::APL),
            "DHCID" => Ok(Self::DHCID),
            "HIP" => Ok(Self::HIP),
            "IPSECKEY" => Ok(Self::IPSECKEY),

            // Service discovery
            "SRV" => Ok(Self::SRV),

            // Email
            "MX" => Ok(Self::MX),
            "SMIMEA" => Ok(Self::SMIMEA),
            "OPENPGPKEY" => Ok(Self::OPENPGPKEY),

            // Informational
            "TXT" => Ok(Self::TXT),
            "HINFO" => Ok(Self::HINFO),
            "RP" => Ok(Self::RP),
            "LOC" => Ok(Self::LOC),

            // Security
            "SSHFP" => Ok(Self::SSHFP),
            "TLSA" => Ok(Self::TLSA),
            "CERT" => Ok(Self::CERT),
            "KX" => Ok(Self::KX),
            "CAA" => Ok(Self::CAA),

            // Misc.
            "MD" => Ok(Self::MD),
            "MF" => Ok(Self::MF),
            "SOA" => Ok(Self::SOA),
            "MB" => Ok(Self::MB),
            "MG" => Ok(Self::MG),
            "MR" => Ok(Self::MR),
            "NULL" => Ok(Self::NULL),
            "WKS" => Ok(Self::WKS),
            "MINFO" => Ok(Self::MINFO),

            // Wildcard
            "*" => Ok(Self::Any),

            // Unknown
            x => x[1..]
                .parse::<u16>()
                .map(Self::Unknown)
                .map_err(|_| Error::UnexpectedValue("class", x.into())),
        }
    }
}

impl Into<u16> for Type {
    fn into(self) -> u16 {
        match self {
            // Meta
            Self::NS => 2,
            Self::CNAME => 5,
            Self::DNAME => 39,
            Self::PTR => 12,
            Self::NAPTR => 35,
            Self::OPT => 41,
            Self::CSYNC => 62,
            Self::IXFR => 251,
            Self::AXFR => 252,
            Self::TKEY => 249,
            Self::TSIG => 250,
            Self::ZONEMD => 63,

            // DNSSEC
            Self::DNSKEY => 48,
            Self::CDNSKEY => 60,
            Self::RRSIG => 46,
            Self::NSEC => 47,
            Self::NSEC3 => 50,
            Self::NSEC3PARAM => 51,
            Self::DS => 43,
            Self::CDS => 59,
            Self::TA => 32768,
            Self::DLV => 32769,

            // IP
            Self::A => 1,
            Self::AAAA => 28,
            Self::APL => 42,
            Self::DHCID => 49,
            Self::HIP => 55,
            Self::IPSECKEY => 45,

            // Service discovery
            Self::SRV => 33,

            // Email
            Self::MX => 15,
            Self::SMIMEA => 53,
            Self::OPENPGPKEY => 61,

            // Informational
            Self::TXT => 16,
            Self::HINFO => 13,
            Self::RP => 17,
            Self::LOC => 29,

            // Security
            Self::SSHFP => 44,
            Self::TLSA => 52,
            Self::CERT => 37,
            Self::KX => 36,
            Self::CAA => 257,

            // Misc.
            Self::MD => 3,
            Self::MF => 4,
            Self::SOA => 6,
            Self::MB => 7,
            Self::MG => 8,
            Self::MR => 9,
            Self::NULL => 10,
            Self::WKS => 11,
            Self::MINFO => 14,

            // Wildcard
            Self::Any => 255,

            // Unknown
            Self::Unknown(x) => x,
        }
    }
}

impl From<u16> for Type {
    fn from(value: u16) -> Self {
        match value {
            // Meta
            2 => Self::NS,
            5 => Self::CNAME,
            39 => Self::DNAME,
            12 => Self::PTR,
            35 => Self::NAPTR,
            41 => Self::OPT,
            62 => Self::CSYNC,
            251 => Self::IXFR,
            252 => Self::AXFR,
            249 => Self::TKEY,
            250 => Self::TSIG,
            63 => Self::ZONEMD,

            // DNSSEC
            48 => Self::DNSKEY,
            60 => Self::CDNSKEY,
            46 => Self::RRSIG,
            47 => Self::NSEC,
            50 => Self::NSEC3,
            51 => Self::NSEC3PARAM,
            43 => Self::DS,
            59 => Self::CDS,
            32768 => Self::TA,
            32769 => Self::DLV,

            // IP
            1 => Self::A,
            28 => Self::AAAA,
            42 => Self::APL,
            49 => Self::DHCID,
            55 => Self::HIP,
            45 => Self::IPSECKEY,

            // Service discovery
            33 => Self::SRV,

            // Email
            15 => Self::MX,
            53 => Self::SMIMEA,
            61 => Self::OPENPGPKEY,

            // Informational
            16 => Self::TXT,
            13 => Self::HINFO,
            17 => Self::RP,
            29 => Self::LOC,

            // Security
            44 => Self::SSHFP,
            52 => Self::TLSA,
            37 => Self::CERT,
            36 => Self::KX,
            257 => Self::CAA,

            // Misc.
            3 => Self::MD,
            4 => Self::MF,
            6 => Self::SOA,
            7 => Self::MB,
            8 => Self::MG,
            9 => Self::MR,
            10 => Self::NULL,
            11 => Self::WKS,
            14 => Self::MINFO,

            // Wildcard
            255 => Self::Any,

            // Unknown
            x => Self::Unknown(x),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Serialize)]
#[repr(u16)]
pub enum Class {
    IN = 1,
    CS = 2,
    CH = 3,
    Any = 255,
    Unknown(u16),
}

impl Display for Class {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IN => write!(f, "{}", "IN"),
            Self::CS => write!(f, "{}", "CS"),
            Self::CH => write!(f, "{}", "CH"),
            Self::Any => write!(f, "{}", "*"),
            Self::Unknown(x) => write!(f, "{}", x),
        }
    }
}

impl FromStr for Class {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "IN" => Ok(Self::IN),
            "CS" => Ok(Self::CS),
            "CH" => Ok(Self::CH),
            "*" => Ok(Self::Any),
            x => x[1..]
                .parse::<u16>()
                .map(Self::Unknown)
                .map_err(|_| Error::UnexpectedValue("type", x.into())),
        }
    }
}

impl Into<u16> for Class {
    fn into(self) -> u16 {
        match self {
            Self::IN => 1,
            Self::CS => 2,
            Self::CH => 3,
            Self::Any => 255,
            Self::Unknown(x) => x,
        }
    }
}

impl From<u16> for Class {
    fn from(value: u16) -> Self {
        match value {
            1 => Self::IN,
            2 => Self::CS,
            3 => Self::CH,
            255 => Self::Any,
            x => Self::Unknown(x),
        }
    }
}

bitflags! {
    #[derive(Clone, Copy, Debug, PartialEq, Eq)]
    pub struct Flags: u16 {
        const AuthoritativeAnswer = 0x0400;
        const Truncation = 0x0200;
        const RecursionDesired = 0x0100;
        const RecursionAvailable = 0x0080;
    }
}

impl Serialize for Flags {
    fn serialize<S>(&self, s: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        s.serialize_u16(self.bits())
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum ResourceData {
    A(Ipv4Addr),
    NS(String),
    CNAME(String),
    SOA { mname: String, rname: String, serial: u32, refresh: u32, retry: u32, expire: u32, minimum: u32 },
    PTR(String),
    SRV { priority: u16, weight: u16, port: u16, target: String },
    MX { preference: u16, exchange: String },
    TXT(String),
    AAAA(Ipv6Addr),
    SSHFP{ algorithm: u8, fptype: u8, fingerprint: String },
    CAA{ flags: u8, tag: String, value: String},
    // DNSKEY { flags: u16, protocol: u8, algorithm: Algorithm, public_key: String },
    // RRSIG { type_covered: Type, algorithm: Algorithm, labels: u8, original_ttl: u32, signature_expiration: u32, signature_inception: u32, key_tag: u16, signer_name: String, signature: String },
    // NSEC { next_domain_name: String, type_bitmap: Vec<u8> },
    // DS { key_tag: u16, algorithm: Algorithm, digest_type: DigestType, digest: Vec<u8> },
    Raw { typ: Type, data: Vec<u8> },
}

impl ResourceData {
    pub fn resource_type(&self) -> Type {
        match self {
            Self::A(_) => Type::A,
            Self::NS(_) => Type::NS,
            Self::CNAME(_) => Type::CNAME,
            Self::SOA { .. } => Type::SOA,
            Self::PTR(_) => Type::PTR,
            Self::SRV { .. } => Type::SRV,
            Self::MX { .. } => Type::MX,
            Self::TXT(_) => Type::TXT,
            Self::AAAA(_) => Type::AAAA,
            Self::SSHFP{..} => Type::SSHFP,
            Self::CAA{..} => Type::CAA,
            Self::Raw { typ, .. } => *typ,
        }
    }
}

impl Display for ResourceData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::A(ipv4) => write!(f, "{}", ipv4),
            Self::NS(name) => write!(f, "{}", name),
            Self::CNAME(name) => write!(f, "{}", name),
            Self::SOA { mname, rname, serial, refresh, retry, expire, minimum } => write!(f, "mname={}, rname={}, serial={}, refresh={}, retry={}, expire={}, minimum={}", mname, rname, serial, refresh, retry, expire, minimum),
            Self::PTR(name) => write!(f, "{}", name),
            Self::SRV { priority, weight, port, target } => write!(f, "priority={}, weight={}, port={}, target={}", priority, weight, port, target),
            Self::MX { preference, exchange } => write!(f, "preference={}, exchange={}", preference, exchange),
            Self::TXT(txt) => write!(f, "{}", txt),
            Self::AAAA(ipv6) => write!(f, "{}", ipv6),
            Self::SSHFP { algorithm, fptype, fingerprint } => write!(f, "algorithm={}, fptype={}, fingerprint={}", algorithm, fptype, fingerprint),
            Self::CAA { flags, tag, value } => write!(f, "flags={}, tag={}, value={}", flags, tag, value),
            Self::Raw { data, .. } => write!(f, "raw<{}>", data.encode_hex::<String>()),
        }
    }
}

impl Serialize for ResourceData {
    fn serialize<S>(&self, s: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut state = s.serialize_struct("resource_data", 2)?;
        state.serialize_field("type", &self.resource_type().to_string())?;
        state.serialize_field("data", &self.to_string())?;
        state.end()
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("format error")]
    FormatError,
    #[error("server failure")]
    ServerFailure,
    #[error("name error")]
    NameError,
    #[error("not implemented")]
    NotImplemented,
    #[error("refused")]
    Refused,
    #[error("unexpected {0}: '{1}'")]
    UnexpectedField(&'static str, u16),
    #[error("unexpected {0} value: {1}")]
    UnexpectedValue(&'static str, String),
    #[error("io error: {0}")]
    IoError(io::Error),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::IoError(err)
    }
}

fn duration_sec<S>(d: &Duration, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_u64(d.as_secs())
}
