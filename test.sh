#!/usr/bin/env bash

test() {
  target/debug/diglett $@;
}

set -x;

test twitter.com
test -t=SRV _imaps._tcp.gmail.com.
test -t=MX gmail.com.
test -t=TXT example.com.
test -t=CAA cloudflare.com.
test -t=SSHFP salsa.debian.org.