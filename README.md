# Diglett
It digs! A DNS resolving CLI.

![image](readme/diglett-aloha.png)

Inspired by the [Implement DNS in a weekend](https://jvns.ca/blog/2023/05/12/introducing-implement-dns-in-a-weekend/) article. 
I read through [RFC 1035](https://datatracker.ietf.org/doc/html/rfc1035) and coded this tool when I was babysitting my newborn daughter in the night.

## What's special about it?
It can output JSON, or colorful text in terminal ;)

## Installation
```bash
cargo install diglett
```

## Example
![image](readme/example.png)
